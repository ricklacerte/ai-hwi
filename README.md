# SYS866

AI project for SYS866 class

## Literature review

[Learning Deep Architectures for AI, Joshua Bengio ](https://www.iro.umontreal.ca/~bengioy/papers/ftml.pdf)

[Siamese Network, Gregory Koch](http://www.cs.utoronto.ca/~gkoch/files/msc-thesis.pdf)

[CNN for image classification](https://pdfs.semanticscholar.org/b8e3/613d60d374b53ec5b54112dfb68d0b52d82c.pdf)

[An analysis of CNN for image classification](https://www.sciencedirect.com/science/article/pii/S1877050918309335)

[ImageNet classification with CNN](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf)

[Bag of tricks for image classification](https://arxiv.org/pdf/1812.01187.pdf)

[CNN Based on 2D Spectrum for Hyperspectral Image Classification](https://www.hindawi.com/journals/js/2018/8602103/)

[Hyperspectral Remote Sensing Image Classification Based on Maximum Overlap Pooling Convolutional Neural Network](https://www.mdpi.com/1424-8220/18/10/3587)

[Deep CNN for image recognition](https://reader.elsevier.com/reader/sd/pii/S1574954118302140?token=1EA6AAEAF0558854FBA7ED8E6DD9401F8E8486CACE90FCC350A94E2C4613575D61020D8E9D03DE9D0D939B7226240635)

## Development guide
1. Create (and activate) a conda virtual environment

    `conda env create -f conda_env.yml`

    `conda activate SYS866`

2. Download Kaggle datasets 

    `cd data`
    
    `kaggle competitions download -c humpback-whale-identification`
    
   `unzip train.zip  -d train`
   
   `unzip test.zip  -d test`
    

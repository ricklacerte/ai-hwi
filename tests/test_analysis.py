import os
import pytest
import pickle
from shutil import rmtree
from HWI.analysis import Analysis

HISTORY_FILE = os.path.join(os.path.dirname(__file__), "HWI_1.0.0_history")

data_ex = {"loss": [1, 2, 3, 4], 'acc': [1, 2, 3, 4]}
model_name = 'test_model'


@pytest.fixture
def analysis_obj():
    """
    :return:
    """
    if os.path.isdir(model_name):
        rmtree(model_name)

    with open(HISTORY_FILE, 'rb') as file_:
        history = pickle.load(file_)

    yield Analysis(history, model_name)


def test_graph_analysis(analysis_obj):
    """

    :param history_data:
    :return:
    """
    analysis_obj.accuracy_graph()
    analysis_obj.loss_graph()

    assert os.path.isdir(model_name)
    assert '{0}_accuracy.jpg'.format(model_name) in os.listdir(model_name)
    assert '{0}_loss.jpg'.format(model_name) in os.listdir(model_name)


def test_save_model(analysis_obj):
    """

    :param history_object:
    :return:
    """
    analysis_obj.save_model()
    assert os.path.isdir(model_name)
    assert '{0}.h5'.format(model_name) in os.listdir(model_name)

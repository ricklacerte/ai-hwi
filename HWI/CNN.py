import os
import keras.models as models
import keras.layers as layers
import keras.optimizers as optimizers
from keras import backend
# from keras.callbacks import EarlyStopping, ModelCheckpoint  # TODO: implement early stop
# from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau  # TODO : Implement changing learning in learning
import keras.losses as losses
from HWI.data import get_train_data, get_train_label, get_test_data, convert_train_img, DATA_FOLDER, TRAIN_DATA_FOLDER
from HWI.analysis import Analysis

assert len(backend.tensorflow_backend._get_available_gpus()) > 0

MODEL_NAME = "HWI_1.0.0"
INPUT_DATA_SIZE = (100, 100, 3)

# To not overwrite previous models
if os.path.isdir(MODEL_NAME):
    raise FileExistsError

# convert_train_img(INPUT_DATA_SIZE)
X = get_train_data(TRAIN_DATA_FOLDER, target_size=INPUT_DATA_SIZE)
Y = get_train_label()

model = models.Sequential()

# TODO : Add normalization layers?
# TODO : Add kernel_regularizer ?
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), name='Conv1', input_shape=X.shape[1::]))
model.add(layers.ReLU(name='act1'))
model.add(layers.AveragePooling2D(pool_size=(2, 2), name="Pool1"))

model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), name='Conv2'))
model.add(layers.ReLU(name='act2'))
# model.add(layers.AveragePooling2D(pool_size=(2, 2), name="Pool2"))

model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), name='Conv3'))
model.add(layers.ReLU(name='act3'))
model.add(layers.MaxPooling2D(pool_size=(2, 2), name="Pool3"))

model.add(layers.Flatten(name='fcn-flat'))

model.add(layers.Dense(units=500, activation='relu', name='fcn-relu'))
model.add(layers.Dropout(rate=0.70))  # TODO Adjust dropout
model.add(layers.Dense(units=Y.shape[1], activation='softmax', name='fcn-sm'))

model.compile(optimizer=optimizers.SGD(lr=0.01),  # TODO Learning Rate decay=
              loss=losses.categorical_crossentropy,
              metrics=['accuracy'])
model.summary()

# Training model
history = model.fit(x=X,
                    y=Y,
                    epochs=100,
                    batch_size=50,  # TODO Adjust batch size according to learning rate
                    validation_split=0.1,
                    shuffle=True,
                    )  # TODO Add lrDecay, regulation with callback=

# Analysis
model_analysis = Analysis(history, MODEL_NAME)
model_analysis.save_model()
model_analysis.save_history()
model_analysis.accuracy_graph()
model_analysis.loss_graph()

# Test
# X = get_test_data(INPUT_DATA_SIZE)
# predictions = model.predict(np.array(X), verbose=1)

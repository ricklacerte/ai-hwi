import os
import csv
from keras.callbacks import History
import numpy as np
import pickle
import matplotlib.pyplot as plt

import pandas as pd


class Analysis():
    """
    Analysis of performance KERAS model

    """

    def __init__(self, history, model_name):
        assert isinstance(history, History)
        self._history = history
        self._model_name = model_name
        self._folder = os.path.join(os.getcwd(), self._model_name)
        os.mkdir(self._folder)

    def save_history(self):
        """
        dumps history to a file (retrieve later)
        """
        filename = os.path.join(self._folder, '{0}_history'.format(self._model_name))
        if os.path.isfile(filename):
            raise FileExistsError

        with open(filename, 'wb') as file_pi:
            pickle.dump(self._history, file_pi)

    def save_model(self):
        """

        Saving model to .h5 file
        :return:
        """
        filename = os.path.join(self._folder, '{0}.h5'.format(self._model_name))
        self._history.model.save(filename)

    def accuracy_graph(self):
        """
        Create accuracy graph from history
        :return:
        """
        plt.clf()
        plt.title("Model {0} Accuracy".format(self._model_name))
        plt.xlabel('epochs')
        plt.ylabel('Accuracy (%)')
        plt.plot(self._history.epoch, self._history.history['acc'], label='train')
        plt.plot(self._history.epoch, self._history.history['val_acc'], label='validation')
        plt.legend()
        plt.savefig(os.path.join(self._folder, '{0}_accuracy.jpg'.format(self._model_name)))

    def loss_graph(self):
        """
        Create Loss graph from history
        :return:
        """
        plt.clf()
        plt.title("Model {0} Loss".format(self._model_name))
        plt.xlabel('epochs')
        plt.ylabel('Loss')
        plt.plot(self._history.epoch, self._history.history['loss'], label='train')
        plt.plot(self._history.epoch, self._history.history['val_loss'], label='validation')
        plt.legend()
        plt.savefig(os.path.join(self._folder, '{0}_loss.jpg'.format(self._model_name)))

import os
import pandas as pd
from keras.preprocessing import image

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import numpy as np

ROOT_FOLDER = os.path.dirname(os.path.dirname(__file__))
DATA_FOLDER = os.path.join(ROOT_FOLDER, 'data')

TRAIN_DATA_FOLDER = os.path.join(DATA_FOLDER, 'train')
TRAIN_TAG_FILE = os.path.join(DATA_FOLDER, 'train.csv')

TEST_DATA_FOLDER = os.path.join(DATA_FOLDER, 'test')
TEST_SUBMISSION_FILE = os.path.join(DATA_FOLDER, 'sample_submission.csv')

# TODO : Delete that! (use because lack of processing power)
NUMBER_TRAIN_IMAGE = 300


# TODO : Data expansion
# image.ImageDataGenerator.horizontal_flip
# image.ImageDataGenerator.brightness_range
# image.apply_brightness_shift

def get_train_data(data_location, target_size=None):
    """
    Fomatting the train data images to a specific size.
    Image must be ALL the same reoslution
    :return:
    """
    train_csv = pd.read_csv(TRAIN_TAG_FILE)
    # train_data = np.zeros(((train_csv['Image'].shape[0], img.size[0], img.size[1], img.layer)))

    # Get resolution
    img = image.load_img(os.path.join(data_location, '{0}'.format(train_csv['Image'][0])))
    if target_size is None:
        train_data = np.zeros((train_csv['Image'].shape[0], img.size[0], img.size[1], img.layers))
    else:
        train_data = np.zeros(((train_csv['Image'].shape[0],) + target_size))

    count = 0
    for img_name in train_csv['Image']:
        load_img = image.load_img(os.path.join(data_location, '{0}'.format(img_name)), target_size=target_size)
        x = image.img_to_array(load_img)
        train_data += x
        if (count % 100 == 0):
            print("Processing Train image: {0} ({1}/{2})".format(img_name, count, train_data.shape[0]))
        count += 1

    return train_data


def convert_train_img(target_size):
    resize_folder = os.path.join(DATA_FOLDER, 'resize_{0}x{1}'.format(target_size[0], target_size[1]))
    os.mkdir(resize_folder)
    train_csv = pd.read_csv(TRAIN_TAG_FILE)
    count = 0
    for img_name in train_csv['Image']:
        load_img = image.load_img(os.path.join(TRAIN_DATA_FOLDER, '{0}'.format(img_name)), target_size=target_size)
        x = image.img_to_array(load_img)
        new_img = image.array_to_img(x)
        new_img.save(os.path.join(resize_folder, "{0}".format(img_name)))
        # train_data += x
        if (count % 100 == 0):
            print("Resizing Train image: {0} ({1}/{2})".format(img_name, count, train_csv['Image'].shape[0]))
        count += 1

    # return train_data


def get_train_label():
    """
    Formatting the train data label for classification
    :return:
    """
    train_csv = pd.read_csv(TRAIN_TAG_FILE)
    train_id = np.array(train_csv['Id'])

    # Convert the tag_id to an integer
    label_encoded = LabelEncoder().fit_transform(train_id)

    # Convert the label encoded to a "classification" vector
    y = OneHotEncoder().fit_transform(label_encoded.reshape(len(label_encoded), 1))

    return y


def get_test_data(target_size):
    """
    Getting the Testing data
    :return:
    """
    test_csv = pd.read_csv(TEST_SUBMISSION_FILE)
    test_csv['Image'].shape
    test_data = np.zeros(((test_csv['Image'].shape[0],) + target_size))
    count = 0
    for img_name in test_csv['Image']:
        load_img = image.load_img(os.path.join(TEST_DATA_FOLDER, '{0}'.format(img_name)), target_size=target_size)
        x = image.img_to_array(load_img)
        test_data += x
        if (count % 100 == 0):
            print("Processing Test image: {0} ({1}/{2})".format(img_name, count, test_data.shape[0]))
        count += 1

    return test_data


def generate_submission_file(predict_result):
    """

    :return:
    """
